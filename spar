#!/usr/bin/env perl

#==============================================================================
# spar
# File ID: 591c2bba-80d8-11e5-9df3-02010e0a6634
#
# [Description]
#
# Character set: UTF-8
# ©opyleft 2015– Øyvind A. Holm <sunny@sunbase.org>
# License: GNU General Public License version 2 or later, see end of file for 
# legal stuff.
#==============================================================================

use strict;
use warnings;
use Getopt::Long;
use IPC::Open3;

local $| = 1;

our %Opt = (

    'above' => 0,
    'add' => '',
    'begin-time' => '',
    'begin-value' => '',
    'below' => 0,
    'colour' => 0,
    'current' => '',
    'directory' => '',
    'end-time' => '',
    'end-value' => '',
    'get-current' => '',
    'goal' => 0,
    'help' => 0,
    'init' => 0,
    'no-colour' => 0,
    'now' => '',
    'per' => '',
    'quiet' => 0,
    'status' => 0,
    'time' => '',
    'verbose' => 0,
    'version' => 0,
    'what' => '',
    'when' => '',

);

our $progname = $0;
$progname =~ s/^.*\/(.*?)$/$1/;
our $VERSION = '0.3.0+git';

our $sql_error = 0; # Set to !0 if some sqlite3 error happened

Getopt::Long::Configure('bundling');
GetOptions(

    'above|A' => \$Opt{'above'},
    'add|a=f' => \$Opt{'add'},
    'begin-time|bt=s' => \$Opt{'begin-time'},
    'begin-value|bv=f' => \$Opt{'begin-value'},
    'below|B' => \$Opt{'below'},
    'colour' => \$Opt{'colour'},
    'current|c=f' => \$Opt{'current'},
    'directory|d=s' => \$Opt{'directory'},
    'end-time|et=s' => \$Opt{'end-time'},
    'end-value|ev=f' => \$Opt{'end-value'},
    'get-current' => \$Opt{'get-current'},
    'goal|g' => \$Opt{'goal'},
    'help|h' => \$Opt{'help'},
    'init' => \$Opt{'init'},
    'no-colour' => \$Opt{'no-colour'},
    'now=s' => \$Opt{'now'},
    'per|p=s' => \$Opt{'per'},
    'quiet|q+' => \$Opt{'quiet'},
    'status|s' => \$Opt{'status'},
    'time|t=s' => \$Opt{'time'},
    'verbose|v+' => \$Opt{'verbose'},
    'version' => \$Opt{'version'},
    'what|W=s' => \$Opt{'what'},
    'when|w=f' => \$Opt{'when'},

) || die("$progname: Option error. Use -h for help.\n");

$Opt{'verbose'} -= $Opt{'quiet'};
$Opt{'help'} && usage(0);
if ($Opt{'version'}) {
    print_version();
    exit(0);
}

my $currjul = '';
if (length($Opt{'now'})) {
    $currjul = date2jul($Opt{'now'});
    if (!length($currjul)) {
        warn("$progname: '$Opt{'now'}': Invalid --now timestamp\n");
        exit(1);
    }
} else {
    $currjul = date2jul('now');
}
msg(2, "currjul = '$currjul'");

# Increase when incompatible changes are made to the database schema
my $current_dbversion = 1;

chomp(my $sql_create_config =
    "CREATE TABLE config (\n"
    . "  name TEXT\n"
    . "    CONSTRAINT config_name_length\n"
    . "      CHECK (length(name) > 0)\n"
    . "    UNIQUE\n"
    . "    NOT NULL,\n"
    . "  value JSON\n"
    . "    CONSTRAINT config_value_valid_json\n"
    . "      CHECK (json_valid(value))\n"
    . "    NOT NULL\n"
    . ");\n"
    . "INSERT INTO \"config\" VALUES('dbversion',$current_dbversion);\n");

exit(main());

sub main {
    my $Retval = 0;
    my $sql = '';
    my %udiv = (
        'se' => 1,
        'mi' => 60,
        'ho' => 3600,
        'da' => 86400,
        'we' => 604800,
        'mo' => 2629800, # ye / 12
        'ye' => 31557600, # da * 365.25
    );
    my ($per_val, $per_unit) = ('', '');

    # Check options and project name
    if (
        length($Opt{'when'})
        && ($Opt{'goal'} || $Opt{'status'} || length($Opt{'time'}))
    ) {
        warn("$progname: Cannot combine --when with --goal,"
             . " --status or --time\n");
        return 1;
    }

    if ($Opt{'above'} && $Opt{'below'}) {
        warn("$progname: Cannot combine -A/--above and -B/--below\n");
        return 1;
    }

    if (!defined($ARGV[0]) || !length($ARGV[0])) {
        warn("No project specified, aborting\n");
        return 1;
    }

    my $proj = $ARGV[0];
    $proj =~ s/\.spar//;

    if (!valid_projname($proj)) {
        warn("$progname: $proj: Invalid project name\n");
        return 1;
    }

    if (length($Opt{'per'})) {
        ($per_val, $per_unit) = parse_per($Opt{'per'});
    }

    # Set up $spardir
    # FIXME: Hardcoding atm
    my $spardir = "$ENV{HOME}/spar";
    if (length($Opt{'directory'})) {
        $spardir = $Opt{'directory'};
    }

    if (!-d $spardir) {
        msg(0, "$spardir: Directory not found, creating it");
        if (!mkdir($spardir)) {
            warn("$progname: $spardir: Could not create directory");
            return 1;
        }
    }

    my $db = "$spardir/$proj.spar";

    if ($Opt{'init'}) {
        if (!init_db("$db")) {
            warn("$progname: $db: Database init failed\n");
            return 1;
        }
    }

    if (!-e $db) {
        warn("$progname: $proj: Project not found\n");
        return 1;
    }

    check_db_version($db, $proj) || return 1;
    update_db_cmdline($db);

    my $currvalue = '';
    chomp($currvalue = sql($db,
        "SELECT current FROM account\n"
        . "  ORDER BY date DESC\n"
        . "  LIMIT 1;\n"));

    if (length($Opt{'add'})) {
        !length($currvalue) && ($currvalue = 0);
        $currvalue += $Opt{'add'};
        sql($db,
            "INSERT INTO account\n"
            . "  (date, current)\n"
            . "  VALUES (datetime($currjul), $currvalue);\n");
    }

    chomp(my $begindate = sql($db, "SELECT begindate FROM goal;"));
    my $beginjul = date2jul($begindate);
    chomp(my $beginvalue = sql($db, "SELECT beginvalue FROM goal;"));
    chomp(my $enddate = sql($db, "SELECT enddate FROM goal;"));
    my $endjul = date2jul($enddate);
    chomp(my $endvalue = sql($db, "SELECT endvalue FROM goal;"));
    chomp(my $below = sql($db, "SELECT value FROM config"
                               . " WHERE name = 'below';"));
    $below = ($below =~ /1/) ? 1 : 0;

    if (length($Opt{'per'})) {
        my $arg = $Opt{'per'};

        if (!length($begindate) || !length($beginvalue)
            || !length($endvalue))
        {
            warn("$progname: Missing one or more of --begin-time,"
                 . " --begin-value or --end-value\n");
            exit(1);
        }
        my $prday = $per_val * 86400 / $udiv{$per_unit};
        if ($endvalue < $beginvalue) {
            $prday = 0 - $prday;
        }
        $endjul = $beginjul + ($endvalue - $beginvalue) / $prday;
        $enddate = jul2date($endjul);
        sql($db,
            "UPDATE goal\n"
            . "  SET enddate = datetime($endjul);\n");
    }

    my $per_day = '';
    my $currgoal = '';

    if (
        length($beginjul) && length($beginvalue)
        && length($endjul) && length($endvalue)
    ) {
        chomp(
            $per_day = interpol(
                $beginjul + 1,
                $beginjul, $beginvalue,
                $endjul, $endvalue
            ) - $beginvalue
        );
        $currgoal = interpol(
            length($Opt{'time'}) ? date2jul($Opt{'time'}) : $currjul,
            $beginjul, $beginvalue,
            $endjul, $endvalue,
        );
    }

    if (!length($currvalue)) {
        $currvalue = $currgoal;
    }

    if (length($Opt{'when'})) {
        if (
            !length($begindate) || !length($beginvalue)
            || !length($enddate) || !length($endvalue)
        ) {
            warn("$progname: Missing begin/end time or begin/end value\n");
            return 1;
        }
        my $goaltime = goal_x(
            $Opt{'when'},
            date2jul($begindate), $beginvalue,
            date2jul($enddate), $endvalue,
        );
        printf("%s, %s\n",
            jul2human($goaltime - $currjul),
            jul2date($goaltime),
        );
        return 0;
    }

    if (length($Opt{'what'})) {
        if ($Opt{'what'} eq "et") {
            if (!length($enddate)) {
                warn("$progname: --et/--end-time value not defined,"
                     . " cannot use \"-W et\"\n");
                exit(1);
            }
        } else {
            $endjul = date2jul($Opt{'what'});
            if (!length($endjul)) {
                warn("$progname: Invalid date in -W/--what option\n");
                exit(1);
            }
        }
        printf("%f\n", interpol($endjul,
                       $beginjul, $beginvalue,
                       $currjul, $currvalue) . "\n");
        return 0;
    }

    msg(1, "Goal started $begindate with value $beginvalue");
    msg(1, "Goal ends at $enddate with value $endvalue");
    if (length($per_day)) {
        msg(1, sprintf("%f/year, %f/month, %f/week",
                       $per_day * 365.25, $per_day * 30.4375, $per_day * 7));
        msg(1, sprintf("%f/day, %f/hour", $per_day, $per_day / 24));
    }
    msg(1, "Current value is '$currvalue'");

    my $currstat = '';
    if (length($currvalue) && length($currgoal)) {
        if ($below) {
            $currstat = $currgoal - $currvalue;
        } else {
            $currstat = $currvalue - $currgoal;
        }
    }
    my $poldate = '';
    my $poldatediff = '';
    if (
        length($currvalue)
        && length($begindate) && length($beginvalue)
        && length($enddate) && length($endvalue)
    ) {
        $poldate = goal_x(
            $currvalue,
            date2jul($begindate), $beginvalue,
            date2jul($enddate), $endvalue,
        );
        $poldatediff = $poldate - $currjul;
    }
    if (length($poldatediff) && length($poldate)) {
        msg(2, sprintf(
            "poldatediff = '$poldatediff' (%s, %s)",
            jul2human($poldatediff),
            jul2date($poldate),
        ));
    }

    if (length($Opt{'time'})) {
        printf("Goal at $Opt{'time'}: %s\n",
            length($currgoal)
                ? sprintf("%f", $currgoal)
                : "NULL",
        );
    } else {
        if (length($currgoal)) {
            printf("%f\n", $currgoal);
        } else {
            print("NULL\n");
        }
    }

    my $t_poldatediff = '';
    my $t_poldatediff_reset = '';

    if ($Opt{'get-current'} || $Opt{'status'}) {
        printf("%s\n", length($currvalue) ? $currvalue : "NULL");
        if ($Opt{'status'}) {
            if (
                length($poldatediff)
                && !$Opt{'no-colour'}
                && (-t STDOUT || $Opt{'colour'})
            ) {
                $t_poldatediff_reset = `tput sgr0`;
                if ($currstat < 0) {
                    $t_poldatediff = `tput bold; tput setaf 1`;
                } else {
                    $t_poldatediff = `tput bold; tput setaf 2`;
                }
            }
            if (length($currstat)) {
                printf("%s%f%s\n",
                    $t_poldatediff,
                    $currstat,
                    $t_poldatediff_reset,
                );
            } else {
                print("NULL\n");
            }
            if (length($poldate) && length($poldatediff)) {
                printf("%s%s%s, %s\n",
                    $t_poldatediff,
                    jul2human($poldatediff),
                    $t_poldatediff_reset,
                    jul2date($poldate),
                );
            } else {
                print("NULL, NULL\n");
            }
        }
    }
    if ($Opt{'goal'}) {
        my ($time_left, $goal_date) = ('NULL', 'NULL');
        my ($goal_reached, $goal_reached_diff) = ('NULL', 'NULL');
        if (length($beginjul) && length($beginvalue)
            && length($currjul) && length($currvalue)) {
            $goal_reached = goal_x(
                $endvalue,
                $beginjul, $beginvalue,
                $currjul, $currvalue,
            );
            if ($goal_reached ne "NULL") {
                $goal_reached_diff = $goal_reached - $currjul;
                $time_left = jul2human($goal_reached_diff);
                $goal_date = jul2date($goal_reached);
            }
        }
        msg(2, "goal_reached = '$goal_reached'");
        msg(2, "goal_reached_diff = '$goal_reached_diff'");
        print("ETA $time_left, $goal_date\n");
    }

    return $Retval;
}

sub check_db_version {
    my ($db, $proj) = @_;

    my $dbversion = config_value($db, 'dbversion');
    msg(3, "config.dbversion = '$dbversion'");
    if (!length($dbversion)) {
        msg(-1, "$proj.spar: config.dbversion not found, cannot continue");
        return 0;
    } else {
        if ($dbversion > $current_dbversion) {
            warn("$progname: Database version is $dbversion\n");
            warn("$progname: This version only supports"
                 . " version $current_dbversion\n");
            return 0;
        }
    }

    return 1;
}

sub parse_per {
    my $arg = shift;
    my ($v, $u);

    if ($arg =~ /^-/) {
        warn("$progname: Negative numbers in -p/--per not allowed\n");
        exit(1);
    }
    if ($arg !~ /^[\d\.]+\/(se|mi|ho|da|we|mo|ye)/i) {
        warn("$progname: Invalid argument in -p/--per option\n");
        exit(1);
    }
    $arg =~ /^([\d\.]+)\/(se|mi|ho|da|we|mo|ye)/i && ($v = $1, $u = lc($2));
    msg(3, "parse_per(): v = \"$v\", u = \"$u\"");
    $v *= 1;
    if (!$v) {
        warn("$progname: Cannot use 0 in -p/--per\n");
        exit(1);
    }

    return ($v, $u);
}

sub update_db_cmdline {
    my $db = shift;
    my $sql = '';

    if (length($Opt{'begin-time'})) {
        $sql .=
            "UPDATE goal\n"
            . "  SET begindate = datetime('$Opt{'begin-time'}');\n";
    }

    if (length($Opt{'begin-value'})) {
        $sql .=
            "UPDATE goal\n"
            . "  SET beginvalue = $Opt{'begin-value'};\n";
    }

    if (length($Opt{'end-time'})) {
        $sql .=
            "UPDATE goal\n"
            . "  SET enddate = datetime('$Opt{'end-time'}');\n";
    }

    if (length($Opt{'end-value'})) {
        $sql .=
            "UPDATE goal\n"
            . "  SET endvalue = $Opt{'end-value'};\n";
    }

    if (length($Opt{'current'})) {
        $sql .=
            "INSERT INTO account\n"
            . "  (date, current)\n"
            . "  VALUES (datetime($currjul), $Opt{'current'});\n";
    }

    if ($Opt{'above'}) {
        $sql .=
            "DELETE FROM config\n"
            . "  WHERE\n"
            . "    name = 'below';\n";
    }

    if ($Opt{'below'}) {
        $sql .=
            "INSERT INTO config\n"
            . "  (name, value)\n"
            . "  VALUES ('below', 1);\n";
    }

    if (length($sql)) {
        $sql = "BEGIN;\n$sql\nCOMMIT;\n";
        sql($db, $sql);
        $sql = '';
    }

    return;
}

sub date2jul {
    my $date = shift;
    chomp(my $retval = sql('', "SELECT julianday('$date');"));

    return $retval;
}

sub goal_x {
    # Return x when y, x0, y0, x1 and y1 are known
    my ($y, # Goal to find timestamp for
        $x0, # Start x
        $y0, # Start y
        $x1, # Current x
        $y1, # Current y
    ) = @_;
    if ($y0 == $y1) {
        # Avoid division by zero
        return "NULL";
    }
    for my $f (@_) {
        !length($f) && return "NULL";
    }
    my $x = $x0 + ($y - $y0) / ($y1 - $y0) * ($x1 - $x0);

    return $x;
}

sub init_db {
    my $db = shift;
    my $retval = 1; # 1 = ok, 0 = error.
    my $b_time_str = length($Opt{'begin-time'})
                        ? "'$Opt{'begin-time'}'"
                        : "datetime($currjul)";
    my $b_val_str = length($Opt{'begin-value'})
                        ? $Opt{'begin-value'}
                        : 0;
    my $e_time_str = length($Opt{'end-time'})
                        ? "'$Opt{'end-time'}'"
                        : "NULL";
    my $e_val_str = length($Opt{'end-value'})
                        ? $Opt{'end-value'}
                        : "NULL";
    msg(0, "Initialising database $db");
    if (-e "$db") {
        msg(0, "$db: Database already exists, init aborted");
        $retval = 0;
    } else {
        sql($db,
            "BEGIN EXCLUSIVE TRANSACTION;\n"
            . "$sql_create_config\n"
            . "CREATE TABLE goal (\n"
            . "  begindate TEXT\n"
            . "    CONSTRAINT begindate_valid\n"
            . "      CHECK (datetime(begindate) IS NOT NULL)\n"
            . "    NOT NULL,\n"
            . "  beginvalue REAL,\n"
            . "  enddate TEXT\n"
            . "    CONSTRAINT enddate_valid\n"
            . "      CHECK (enddate IS NULL OR datetime(enddate) IS NOT NULL),\n"
            . "  endvalue REAL\n"
            . ");\n"
            . "CREATE TABLE account (\n"
            . "  date TEXT\n"
            . "    CONSTRAINT date_valid\n"
            . "      CHECK (datetime(date) IS NOT NULL)\n"
            . "    NOT NULL,\n"
            . "  current REAL\n"
            . ");\n"
            . "INSERT INTO goal VALUES ($b_time_str, $b_val_str, $e_time_str, $e_val_str);\n"
            . "COMMIT;\n");
    }

    return $retval;
}

sub interpol {
    my ($x, $x0, $y0, $x1, $y1) = @_;
    my $retval = $y0 + ($y1 - $y0) * ($x - $x0) / ($x1 - $x0);

    return $retval;
}

sub jul2date {
    my $jul = shift;
    chomp(my $retval = sql('', "SELECT datetime($jul);"));

    return $retval;
}

sub jul2human {
    # FIXME: Probably inaccuracies in floating point.
    # The difference between "2015-01-01 12:00:00" and "2015-01-01 
    # 14:00:00" is shown as "0d:01:59:59". Good enough for now.
    my $origjul = 1.0 * shift;
    my $jul = 1.0 * abs($origjul);
    my $rem = 1.0 * $jul;
    my $days = 1.0 * int($rem);
    $rem -= 1.0 * $days;
    my $hours = 1.0 * int($rem * 24.0);
    $rem -= 1.0 * ($hours / 24.0);
    my $minutes = 1.0 * int($rem * 1440.0);
    $rem -= 1.0 * ($minutes / 1440.0);
    my $seconds = 1.0 * int($rem * 86400.0);
    my $retval = sprintf("%s%ud:%02u:%02u:%02u",
        $origjul < 0 ? "-" : "",
        $days, $hours, $minutes, $seconds,
    );

    return $retval;
}

sub valid_projname {
    my $name = shift;
    my $retval = 1;
    $name =~ /\s/ && ($retval = 0);

    return $retval;
}

sub print_version {
    # Print program version
    print("$progname $VERSION\n");

    return;
}

sub config_value {
    my ($db, $name) = @_;
    chomp(my $retval = sql($db,
        "SELECT value\n"
        . "  FROM config\n"
        . "  WHERE\n"
        . "    name = '$name';\n"));

    return $retval;
}

sub sql {
    my ($db, $sql) = @_;
    my @retval = ();

    msg(5, "sql(): db = '$db'");
    local(*CHLD_IN, *CHLD_OUT, *CHLD_ERR);

    $sql_error = 0;
    my $pid = open3(*CHLD_IN, *CHLD_OUT, *CHLD_ERR, "sqlite3", $db) or (
        $sql_error = 1,
        msg(0, "sql(): open3() error: $!"),
        return "sql() error",
    );
    msg(5, "sql(): sql = '$sql'");
    print(CHLD_IN "$sql\n") or msg(0, "sql(): print CHLD_IN error: $!");
    close(CHLD_IN);
    @retval = <CHLD_OUT>;
    msg(5, "sql(): retval = '" . join('|', @retval) . "'");
    my @child_stderr = <CHLD_ERR>;
    if (scalar(@child_stderr)) {
        msg(1, "sqlite3 error: " . join('', @child_stderr));
        $sql_error = 1;
    }

    return join('', @retval);
}

sub usage {
    # Send the help message to stdout
    my $Retval = shift;

    if ($Opt{'verbose'}) {
        print("\n");
        print_version();
    }
    print(<<"END");

Usage: $progname [options]

Options:

  -A, --above
    Stay above the line. This is the default after --init.
  -a AMOUNT, --add AMOUNT
    Add AMOUNT to the current value in the database. Can also be a 
    negative number.
  -B, --below
    Stay below the line.
  --begin-time TIME, --bt TIME
    Define start date, ISO format and UTC.
  --begin-value VAL, --bv VAL
    Numeric start value.
  -c VALUE, --current VALUE
    Update the database with VALUE as the current value.
  --colour
    Use colours even if the output is not a tty. Default action is to 
    use colours (red or green depending on whether you're behind or 
    ahead of schedule) if stdout is a tty, and disable colours if stdout 
    is redirected or used in a pipe.
  -d DIR, --directory DIR
    Store database files in directory DIR.
  --end-time TIME, --et TIME
    Define end date, ISO format and UTC.
  --end-value VAL, --ev VAL
    Numeric end value.
  --get-current
    Display the current value stored in the database.
  -g, --goal
    Display estimated time when the goal will be reached.
  -h, --help
    Show this help.
  --init DBNAME
    Initialise database DBNAME.
  --no-colour
    Disable colours, overrides --colour.
  --now DATETIME
    Use DATETIME as current point in time instead of current time.
  -p/--per VALUE/{sec|min|hour|day|month|year}
    Calculate and set end time by specifying rate of progress.
      1 year = 1 astronomical year (365.25d)
      1 month = year / 12 (30d:10h:30m)
    Only the the two first letters are parsed, so e.g. using "ye" for 
    "year" is ok.
  -q, --quiet
    Be more quiet. Can be repeated to increase silence.
  -s, --status
    Display current status.
  -t TIME, --time TIME
    Display status at TIME, ISO format and UTC.
  -v, --verbose
    Increase level of verbosity. Can be repeated.
  -W TIME, --what TIME
    Display what the value would be with the current progress rate at 
    TIME. To use the end time specified by --end-time or --et, use "et" 
    as parameter value.
  -w VALUE, --when VALUE
    Display point in time when the expected goal is VALUE and exit.
  --version
    Print version information.

END
    exit($Retval);
}

sub msg {
    # Print a status message to stderr based on verbosity level
    my ($verbose_level, $Txt) = @_;

    if ($Opt{'verbose'} >= $verbose_level) {
        print(STDERR "$progname: $Txt\n");
    }

    return;
}

__END__

# This program is free software; you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by the Free Software 
# Foundation; either version 2 of the License, or (at your option) any later 
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with 
# this program.
# If not, see L<http://www.gnu.org/licenses/>.

# vim: set fenc=UTF-8 ft=perl ts=4 sw=4 sts=4 et fo+=w :
